<center><h1>CSCI538 TheExamArtist</h1> <br>
This is the repository for CSCI 538 - AR VR class project.<br>
<h1><a href="https://pthammas.github.io/theexamartist-website" >Our Website </a></h1>

<h4>What is the Exam Artist?</h4>
<p>The Exam Artist is a VR Stealth game where you are a student who tried to finish an exam you know nothing of with various ways and without getting caught by teacher. Created in Unity VR</p>


<h4>How to play</h4>
<p>Use Oculus Controller to point and trigger to select answer/pick up projectile. Various buttons to use skills. (SteamVR required). You can also use mouse left click and drag around to click. Spacebar to show skills.</p>


